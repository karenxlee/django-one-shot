from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem
from django.db.models import Count


# Create your views here.


def todo_list_view(request):
    todo_list = TodoList.objects.all().annotate(items_count=Count("items"))

    context = {
        "todolist_object": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_details = TodoList.objects.get(id=id)
    context = {
        "list_details": list_details,
    }
    return render(request, "todos/detail.html", context)
